import React, { useState, useEffect } from "react";
import { getUserProfile, updateUserProfile, getDownloadsUserProfile } from "../../api/userProfileApi";
import SoftwareItem from "../../components/SoftwareItem/SoftwareItem";
import LicenseManager from "../../components/LicenseManager/LicenseManager"; // Importando LicenseManager
import { useUser } from "../../context/UserContext";
import "./UserProfile.css";

function UserProfile() {
  const { user } = useUser();

  const [profile, setProfile] = useState({
    nome: "",
    email: user.email,
    universidade: "",
    empresa: "",
    titulacaoMaxima: "",
    telefone: "",
  });
  const [isEditing, setIsEditing] = useState(false);
  const [userDownloads, setUserDownloads] = useState([]);

  useEffect(() => {

    const fetchProfile = async () => {
      const userProfile = await getUserProfile();
      setProfile({ ...userProfile, email: user.email });
    };

    const fetchDownloads = async (userId) => {
      if (userId) {
        const downloads = await getDownloadsUserProfile(userId);
        setUserDownloads(downloads);
      }
    };

    fetchProfile();
    if (user.uid) {
      fetchDownloads(user.uid);
    }
  }, [user.uid]);

  const handleChange = (e) => {
    setProfile({ ...profile, [e.target.name]: e.target.value });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    await updateUserProfile(profile);
    setIsEditing(false);
  };

 
  return (
    <div className="user-profile">
      <h1>Perfil do Usuário</h1>

      {isEditing ? (
        <form onSubmit={handleSubmit}>
          <label>
            Nome:
            <input type="text" name="nome" value={profile.nome} onChange={handleChange} required />
          </label>
          <label>
            Email:
            <input type="email" name="email" value={profile.email} disabled />
          </label>
          <label>
            Titulação Máxima:
            <select name="titulacaoMaxima" value={profile.titulacaoMaxima} onChange={handleChange} required>
              <option value="">Selecione</option>
              <option value="PosDoutorado">Pós-Doutorado</option>
              <option value="Doutorado">Doutorado</option>
              <option value="Mestrado">Mestrado</option>
              <option value="Graduacao">Graduação</option>
            </select>
          </label>
          <label>
            Universidade:
            <input type="text" name="universidade" value={profile.universidade} onChange={handleChange} disabled={profile.empresa} />
          </label>
          <label>
            Empresa:
            <input type="text" name="empresa" value={profile.empresa} onChange={handleChange} disabled={profile.universidade} />
          </label>
          <label>
            Telefone:
            <input type="text" name="telefone" value={profile.telefone} onChange={handleChange} />
          </label>
          <button type="submit">Salvar Alterações</button>
        </form>
      ) : (
        <>
          <p>Nome: {profile.nome}</p>
          <p>Email: {profile.email}</p>
          <p>Titulação Máxima: {profile.titulacaoMaxima}</p>
          <p>Universidade: {profile.universidade}</p>
          <p>Empresa: {profile.empresa}</p>
          <p>Telefone: {profile.telefone}</p>
          <button onClick={() => setIsEditing(true)}>Editar Perfil</button>

          {/* Listagem de Softwares Baixados */}
          <div className="user-softwares">
            <h2>Softwares Baixados</h2>

            {/* LicenseManager adicionado logo abaixo do título */}
            <LicenseManager />

            <div className="softwareDownload-list">
              {userDownloads.map((download) => (
                <SoftwareItem key={download.softwareName} software={download} />
              ))}
            </div>
          </div>
        </>
      )}
    </div>
  );
}

export default UserProfile;
