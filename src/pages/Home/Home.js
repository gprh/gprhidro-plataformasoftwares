import React, { useState } from 'react';
import './Home.css';
import SoftwareList from '../../components/SoftwareList/SoftwareList';
import UserProfile from '../UserProfile/UserProfile';
import SoftwareListDrive from '../../components/SoftwareList/SoftwareListDrive';
import Sobre from '../Sobre/Sobre';
import { useUser } from "../../context/UserContext";
import { useNavigate } from "react-router-dom";
import { signOut } from "firebase/auth"; // Importar signOut do Firebase
import { auth } from "../../firebaseConfig";
import AdminDashboard from '../../components/AdminDashboard/AdminDashboard';

function Home() {
  const [selectedContent, setSelectedContent] = useState('softwares');
  const { user, setUser } = useUser();

  const userName = user.email;
 // const isAdmin = user?.email === 'joserui.ufv@gmail.com';
  let isAdmin = false;
  if(userName =='joserui.ufv@gmail.com')
    isAdmin=true;
 
  const navigate = useNavigate();

  const handleLogout = async () => {
    try {
      await signOut(auth); // Desconecta o usuário no Firebase
      setUser(null); // Atualiza o estado do usuário no contexto
      navigate("/login"); // Redireciona para a página de login
    } catch (err) {
      console.error("Erro ao tentar fazer logout:", err);
    }
  };


  // Função para abrir o link do grupo de pesquisa
  const openResearchGroup = () => {
    window.open('https://sites.google.com/view/gprhidro/', '_blank');
  };

  return (
    <div className="home">
      <div className="top-menu">
        <ul>
          <li onClick={() => setSelectedContent('profile')}>Perfil</li>
          <li onClick={() => setSelectedContent('softwares')}>Softwares</li>
          <li onClick={() => setSelectedContent('sobre')}>Sobre</li>
          <li onClick={openResearchGroup}>Site do GPRHidro</li>         
          {isAdmin && <li onClick={() => setSelectedContent('adminDash')}>Dash</li>}
          
        </ul>
        {/* Adiciona uma div para o nome do usuário e botão de logout */}
        <div className="user-logout">
          <span className="user-name">{userName}</span>
          <button onClick={handleLogout} className="logout-button">Logout</button>
        </div>
      </div>
      <div className="content">
        {selectedContent === 'profile' && <UserProfile />}
        {/*selectedContent === 'softwares' && <SoftwareList />*/}
        {selectedContent === 'softwares' && <SoftwareListDrive />}
        {selectedContent === 'sobre' && <Sobre />}        
        {isAdmin && selectedContent === 'adminDash' && <AdminDashboard />}
      
      </div>
    </div>
  );
}

export default Home;
