class Projeto {
    constructor(nome = '', tipo = 'streamflow', data = new Date().toISOString().substring(0, 10), alterado = false, somenteConsistidos = false) {        
        this.nome = nome;
        this.tipo = tipo;
        this.data = data;
        this.alterado = alterado;
        this.somenteConsistidos = somenteConsistidos;
    }
}

export default Projeto;
