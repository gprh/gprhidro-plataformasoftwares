import React, { useState, useEffect } from 'react';
import Modal from 'react-modal';
import { getProjetos } from '../../../../api/projetosApi';
import { useUser } from '../../../../context/UserContext';

Modal.setAppElement('#root');

const ProjetoSelectDialog = ({ isOpen, onRequestClose }) => {
    const [projetos, setProjetos] = useState([]);
    const { currentProject, setCurrentProject } = useUser();

    useEffect(() => {
        const fetchData = async () => {
            const data = await getProjetos();
            setProjetos(data);
        };

        fetchData();
    }, []);

    const handleSelectProject = (projeto) => {
        setCurrentProject(projeto);
        onRequestClose();
    };            


    return (
        <Modal
            isOpen={isOpen}
            onRequestClose={onRequestClose}
            contentLabel="Selecione um Projeto"
            className="projeto-modal"
        >
            <div className="projeto-dialog-header">
                <h2>Selecione um Projeto</h2>
                <button onClick={onRequestClose} className="close-button">&times;</button>
            </div>
            <ul>
                {projetos.map(projeto => (
                    <li key={projeto.id}>
                        {projeto.nome}
                        <button onClick={() => handleSelectProject(projeto)}>Selecionar</button>
                    </li>
                ))}
            </ul>
        </Modal>
    );
};

export default ProjetoSelectDialog;
