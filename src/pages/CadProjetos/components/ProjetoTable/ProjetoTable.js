// src/components/ProjetoTable.js

import React from 'react';
import './ProjetoTable.css';
import { FaEdit, FaTrash } from 'react-icons/fa'; // Importando ícones

const ProjetoTable = ({ projetos, onEdit, onDelete, onSelect }) => {
    if (!projetos || projetos.length === 0) {
        return <p>Nenhum projeto encontrado.</p>;
    }

    return (
        <table className="projeto-table">
            <thead>
                <tr>
                    <th>Nome</th>
                    <th>Tipo</th>
                    <th>Data</th>
                    <th>Somente Consistidos</th>
                    <th>Ações</th>
                    <th>Iniciar</th>
                </tr>
            </thead>
            <tbody>
                {projetos.map(projeto => (
                    <tr key={projeto.id}>
                        <td>{projeto.nome}</td>
                        <td>{projeto.tipo}</td>
                        <td>{projeto.data}</td>
                        <td>{projeto.somenteConsistidos ? 'Sim' : 'Não'}</td>
                        <td>
                            <FaEdit className="icon-edit" onClick={() => onEdit(projeto)} />
                            <FaTrash className="icon-remove" onClick={() => onDelete(projeto.id)} />
                        </td>
                        <td>
                            <button onClick={() => onSelect(projeto)}>Iniciar Projeto</button>
                        </td>

                    </tr>
                ))}
            </tbody>
        </table>
    );
}

export default ProjetoTable;
