import React, { useState, useEffect } from "react";
import Modal from "react-modal";
import ProjetoTable from "./components/ProjetoTable";
import ProjetoForm from "./components/ProjetoForm";
import {
  getProjetos,
  createProjeto,
  updateProjeto,
  deleteProjeto
} from "../../api/projetosApi";
import "./CadProjetos.css";
import { useNavigate } from 'react-router-dom';
import { useUser } from "../../context/UserContext";

Modal.setAppElement("#root");

function CadastroProjetos() {
  const [projetos, setProjetos] = useState([]);
  const [editingProjeto, setEditingProjeto] = useState(null);
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [errorMessage, setErrorMessage] = useState(null);
  const { currentProject, setCurrentProject } = useUser();
  const navigate = useNavigate();

  useEffect(() => {
    const fetchData = async () => {
      const data = await getProjetos();
      setProjetos(data);
    };

    fetchData();
  }, []);

  const handleAddOrUpdate = async (projeto) => {
    try {
      if (editingProjeto) {
        const updatedProjeto = await updateProjeto(editingProjeto.id, projeto);
        setProjetos(
          projetos.map((p) => p.id === editingProjeto.id ? updatedProjeto : p)
        );
        setEditingProjeto(null);
      } else {
        const newProjeto = await createProjeto(projeto);
        if (newProjeto) {
          setProjetos([...projetos, newProjeto]);
        }
      }
      setIsModalOpen(false);
      setErrorMessage(null); // Clear error if operation was successful
    } catch (error) {
      setErrorMessage(error.message);
    }
  };

  const handleDelete = async (id) => {
    const confirmDelete = window.confirm(
      "Você tem certeza de que deseja excluir este projeto?"
    );
    if (confirmDelete) {
      await deleteProjeto(id);
      setProjetos(projetos.filter((p) => p.id !== id));
    }
  };

  const handleEdit = (projeto) => {
    setEditingProjeto(projeto);
    setIsModalOpen(true);
  };

  const handleSelectProject = (projeto) => {
    setCurrentProject(projeto); // Atualiza o projeto atual no contexto
    navigate("/home"); // Redireciona para a página home
  };

  return (
    <div className="cadastro-projetos">
      <h1 className="title"> Projetos Hidrológicos </h1>

      <button className="add-btn" onClick={() => setIsModalOpen(true)}>
        Novo Projeto
      </button>

      <Modal
        isOpen={isModalOpen}
        onRequestClose={() => setIsModalOpen(false)}
        contentLabel="Adicionar Projeto"
      >
        <div className="modal-content">
          <ProjetoForm
            onSubmit={handleAddOrUpdate}
            projeto={editingProjeto} />
          <button className="close-btn" onClick={() => setIsModalOpen(false)}>
            Fechar
          </button>
        </div>
      </Modal>

      <ProjetoTable
        projetos={projetos}
        onEdit={handleEdit}
        onDelete={handleDelete}
        onSelect={handleSelectProject} />
    </div>
  );
}

export default CadastroProjetos;
