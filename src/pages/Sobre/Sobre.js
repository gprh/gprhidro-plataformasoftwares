import React from 'react';
import './Sobre.css'; // Certifique-se de criar este arquivo CSS para estilização
import GPRHidroImagem from '../../assets/GPRHidro-img.jpg';

const Sobre = () => {

  // Função para abrir o link do grupo de pesquisa
  const openResearchGroup = () => {
    window.open('https://sites.google.com/view/gprhidro/', '_blank');
  };


  return (
    <div className="sobre-container">
      <img src={GPRHidroImagem} alt="GPRHidro" className="sobre-imagem"  onClick={openResearchGroup}  />
      <p>
        O GPRHidro é um projeto inovador focado no desenvolvimento de soluções hidrológicas avançadas. Nossa missão é fornecer ferramentas e análises precisas para apoiar a pesquisa e a gestão de recursos hídricos. Com uma equipe de especialistas altamente qualificados e uma abordagem interdisciplinar, estamos na vanguarda do desenvolvimento tecnológico no campo da hidrologia.
      </p>
      <p>
        Nossos softwares são o resultado de décadas de pesquisa intensiva, combinando conhecimento teórico com aplicações práticas. Eles são projetados para serem intuitivos, confiáveis e robustos, atendendo às necessidades de pesquisadores, estudantes e profissionais do setor.
      </p>
      <p>
        Acreditamos na importância de colaborar e compartilhar conhecimento para enfrentar os desafios globais na gestão de recursos hídricos. O GPRHidro está comprometido em contribuir para um futuro mais sustentável e resiliente.
      </p>
    </div>
  );
};

export default Sobre;
