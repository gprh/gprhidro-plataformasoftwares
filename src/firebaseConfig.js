import { initializeApp } from 'firebase/app';
import { getFirestore } from 'firebase/firestore';
import { getStorage } from 'firebase/storage';
import { getAuth } from "firebase/auth";

// const firebaseConfig = {
//   apiKey: "AIzaSyBfux3Czpppvg-Hd-eXuCYu5lv-sI896I4",
//   authDomain: "plataforma-gprhidro.firebaseapp.com",
//   projectId: "plataforma-gprhidro",
//   storageBucket: "plataforma-gprhidro.appspot.com",
//   messagingSenderId: "469540897358",
//   appId: "1:469540897358:web:3571610914b9957c44f91f"
// };

const firebaseConfig = {
  apiKey: "AIzaSyD0MmjSCApBBUD7IUt-U7GcHPnu_YoAWUQ",
  authDomain: "gprhidro-softwares.firebaseapp.com",
  projectId: "gprhidro-softwares",
  storageBucket: "gprhidro-softwares.appspot.com",
  messagingSenderId: "452940083830",
  appId: "1:452940083830:web:a7c29ea7218ca363f2a557"
};

const app = initializeApp(firebaseConfig);

//Para usar o Firetore
const db = getFirestore(app);

//para fazer autenticacao
const auth = getAuth(app);

export { db, auth };
