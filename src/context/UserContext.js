import React, { createContext, useContext, useState } from 'react';

const UserContext = createContext(null);

export const UserProvider = ({ children }) => {
    const [user, setUser] = useState(null);
    const [currentProject, setCurrentProject] = useState(null);

    return (
        <UserContext.Provider value={{ user, setUser, currentProject, setCurrentProject }}>
            {children}
        </UserContext.Provider>
    );
};

export const useUser = () => useContext(UserContext);
