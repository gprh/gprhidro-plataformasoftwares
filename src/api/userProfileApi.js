import { db, auth } from "../firebaseConfig";
import { doc, setDoc, getDoc, updateDoc, getDocs, collection } from "firebase/firestore";
import { updateProfile, updateEmail, reauthenticateWithCredential, EmailAuthProvider } from "firebase/auth";
import { getFirestore, query, where } from "firebase/firestore";

export const getUserProfile = async () => {
  try {
    const user = auth.currentUser;
    const docRef = doc(db, "users", user.uid);
    const docSnap = await getDoc(docRef);

    if (docSnap.exists()) {
      return { uid: user.uid, email: user.email, ...docSnap.data() };
    } else {
      console.log("No such document!");
      return null;
    }
  } catch (error) {
    console.error("Erro ao buscar perfil do usuário:", error);
    return null;
  }
};

export const updateUserProfile = async (profileData) => {
  try {
    const user = auth.currentUser;

    console.log("updateProfile:", user);

    // Atualizar o perfil no Firebase Auth
    await updateProfile(user, {
      displayName: profileData.nome
    });

    // Atualizar o e-mail no Firebase Auth, se necessário
    if (user.email !== profileData.email) {
      const credential = EmailAuthProvider.credential(
        user.email,
        profileData.currentPassword // Necessário para reautenticação
      );
      await reauthenticateWithCredential(user, credential);
      await updateEmail(user, profileData.email);
    }

    // Atualizar o perfil no Firestore
    const userRef = doc(db, "users", user.uid);
    await setDoc(userRef, profileData, { merge: true });

    console.log("Perfil do usuário atualizado com sucesso");
    return profileData;
  } catch (error) {
    console.error("Erro ao atualizar perfil do usuário:", error);
    return null;
  }
};

export const getDownloadsUserProfile = async (userId) => {
  const firestore = getFirestore();
  const downloadsQuery = query(
    collection(firestore, "downloads"),
    where("userId", "==", userId)
  );

  const querySnapshot = await getDocs(downloadsQuery);
  const downloadsData = querySnapshot.docs.map(doc => doc.data());

  // Filtrar para mostrar apenas o último download de cada software
  const uniqueDownloads = {};
  downloadsData.forEach(download => {
    uniqueDownloads[download.softwareName] = download;
  });

  return Object.values(uniqueDownloads);
};

export const addUser = async (userId, email) => {
  const firestore = getFirestore();
  const userProfileRef = doc(firestore, "users", userId);

  try {
    await setDoc(userProfileRef, {
      nome: "",
      email: email,
      universidade: "",
      empresa: "",
      titulacaoMaxima: "",
      // Adicione outros campos conforme necessário
    });
  } catch (error) {
    console.error("Erro ao adicionar usuário:", error);
  }
};

// Função para buscar a data de validade no Firestore
export const getValidationDateOld = async () => {
  try {
    const firestore = getFirestore();
    const configRef = doc(firestore, "config", "valid"); // Documento "valid" na coleção "config"
    const docSnap = await getDoc(configRef);

    if (docSnap.exists()) {
      const data = docSnap.data();
      return data.date; // Retorna a data salva no Firestore
    } else {
      console.warn("Documento de configuração não encontrado!");
      return null;
    }
  } catch (error) {
    console.error("Erro ao buscar data de validade:", error);
    return null;
  }
};


export const getValidationDate = async () => {
  try {
    const firestore = getFirestore();
    const configRef = doc(firestore, "config", "valid"); 
    const docSnap = await getDoc(configRef);

    if (docSnap.exists()) {
      let data = docSnap.data().date;

      // Removendo aspas duplas extras, caso existam
      if (typeof data === "string") {
        data = data.replace(/['"]+/g, "").trim();
      }

      return data;
    } else {
      console.warn("Documento de configuração não encontrado!");
      return null;
    }
  } catch (error) {
    console.error("Erro ao buscar data de validade:", error);
    return null;
  }
};

