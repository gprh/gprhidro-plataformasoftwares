import React, { useState } from 'react';
import UserDownloadsList from '../UserDownloadsList/UserDownloadsList';
import UserList from '../UserList/UserList';

const AdminDashboard = () => {
  const [activeTab, setActiveTab] = useState('userDownloads');

  return (
    <div className="admin-dashboard">
      <div className="tabs">
        <button onClick={() => setActiveTab('userDownloads')} className={activeTab === 'userDownloads' ? 'active' : ''}>Usuários e Downloads</button>
        <button onClick={() => setActiveTab('userList')} className={activeTab === 'userList' ? 'active' : ''}>Ativos</button>
      </div>

      {activeTab === 'userDownloads' && <UserDownloadsList />}
      {activeTab === 'userList' && <UserList />}
    </div>
  );
};

export default AdminDashboard;
