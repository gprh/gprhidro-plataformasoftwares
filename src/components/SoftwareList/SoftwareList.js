import React from 'react';
import './SoftwareList.css';
import { getStorage, ref, getDownloadURL } from "firebase/storage"; // Importações adicionadas aqui
import { getFirestore, doc, serverTimestamp, setDoc, collection } from "firebase/firestore";
import { auth } from "../../firebaseConfig";

import imgHydrologyPlus from '../../assets/softwares/hydrologyImage.png'; 
import imgDHMinas from '../../assets/softwares/DHMinas.png';

const softwareList = [
  // Adicione informações dos softwares aqui
  { id: 1, name: 'HydrologyPlus', imageUrl: imgHydrologyPlus, fileName: 'Software_HydrologyPlus_2024.zip' },
  { id: 2, name: 'DHMinas', imageUrl: imgDHMinas, fileName: 'Software_'  },
  // ...
];

const registerDownload = async (userId, softwareName, temLicenca) => {
  const firestore = getFirestore();
  const downloadRef = doc(collection(firestore, 'downloads'));

  try {
    await setDoc(downloadRef, {
      userId,
      softwareName,
      temLicenca,
      timestamp: serverTimestamp()
    });
    console.log('Download registrado com sucesso');
  } catch (error) {
    console.error('Erro ao registrar o download:', error);
  }
};

// Função para lidar com o download dos softwares
const handleDownload = async (fileName, softwareName, temLicenca) => {
  // Certifique-se de que o usuário está logado
  if (auth.currentUser) {
    // Registra o download
    await registerDownload(auth.currentUser.uid, softwareName, temLicenca);

    // Prossiga com o processo de download
    const storage = getStorage();
    const fileRef = ref(storage, `Softwares GPRHidro/${fileName}`);
    getDownloadURL(fileRef)
      .then((url) => {
        // Cria um link para iniciar o download
        const link = document.createElement('a');
        link.href = url;
        link.setAttribute('download', fileName);
        document.body.appendChild(link);
        link.click();
        link.parentNode.removeChild(link);
      })
      .catch((error) => {
        console.error("Erro ao obter o URL de download:", error);
      });
  } else {
    alert('Usuário não logado.');
  }
};

const SoftwareList = () => {
  return (
    <div className="container">
      <div className="header">
        <h1>Softwares</h1>
        <p>Os softwares produzidos pelo GPRHidro são resultado de avançadas pesquisas realizadas ao longo de décadas, sendo advindos de trabalhos de conclusão de curso, dissertações de mestrado, teses de doutorado e artigos ciêntificos.
            Os softwares estão gratuitos para download. Para efetuar o download é necessário o cadastro de um email válido.</p>       
      </div>
      <div className="grid">
        {softwareList.map((software) => (
          <div key={software.id} className="software-item">
            <img src={software.imageUrl} alt={software.name} />
            {/* Atualize o botão para chamar handleDownload com o nome do arquivo */}
            <button onClick={() => handleDownload(software.fileName, software.name, software.temLicenca)}>Download</button>

          </div>
        ))}
      </div>
    </div>
  );
};

export default SoftwareList;
