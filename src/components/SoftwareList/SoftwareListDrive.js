import React from 'react';
import './SoftwareList.css';

import imgHydrologyPlus from '../../assets/softwares/hydrologyImage.png';
import imgDHMinas from '../../assets/softwares/DHMinas.png';
import imgAperteAqui from '../../assets/softwares/AperteAqui.png';
import imgEstradas from '../../assets/softwares/Estradas.PNG';
import imgTerraco4_1 from '../../assets/softwares/terraço4_1Image.png';

import { getFirestore, doc, serverTimestamp, setDoc, collection } from "firebase/firestore";
import { auth } from "../../firebaseConfig";

const softwareList = [
    { 
        id: 1,
        name: 'HydrologyPlus',
        imageUrl: imgHydrologyPlus, 
        temLicenca: true,
        driveUrl: 'https://drive.google.com/drive/u/4/folders/11tSsl_3ToPmj2jUf2epuvrRMEhWGUUfK' 
    },
    {
        id: 2,
        name: 'DHMinas', 
        imageUrl: imgDHMinas, 
        temLicenca: true,
        driveUrl: 'https://drive.google.com/drive/folders/10Q8mBYYE86t1MCzu8IrpXkvCnl4FarCT?usp=drive_link' 
    },
    {
        id: 3,
        name: 'AperteAqui', 
        imageUrl: imgAperteAqui, 
        temLicenca: false,
        driveUrl: 'https://drive.google.com/drive/folders/1jwvV9MVBOawOzjf3mBUPikKK6WV_s95U?usp=drive_link' 
    },
    {
        id: 4,
        name: 'Estradas', 
        imageUrl: imgEstradas, 
        temLicenca: false,
        driveUrl: 'https://drive.google.com/drive/folders/1PHmDUVvzsSnBZQGm3Kgjabaxl_kMMCE7?usp=drive_link' 
    },
    {
        id: 5,
        name: 'Terraço_4.1', 
        imageUrl: imgTerraco4_1, 
        temLicenca: false,
        driveUrl: 'https://drive.google.com/drive/folders/11PbViWz_DFLEeWpwXVsrl4NZP9-E7lTz?usp=drive_link' 
    },
];


const registerDownload = async (userId, softwareName, temLicenca) => {
    const firestore = getFirestore();
    const downloadRef = doc(collection(firestore, 'downloads'));

    try {
        await setDoc(downloadRef, {
            userId,
            softwareName,
            temLicenca,
            timestamp: serverTimestamp()
        });
        console.log('Download registrado com sucesso');
    } catch (error) {
        console.error('Erro ao registrar o download:', error);
    }
};


// Função para lidar com o download dos softwares
const handleDownload = async (driveUrl, softwareName, temLicenca) => {
    // Certifique-se de que o usuário está logado
    if (auth.currentUser) {
        // Registra o download
        await registerDownload(auth.currentUser.uid, softwareName, temLicenca);

        // Redireciona o usuário para o link do Google Drive
        window.open(driveUrl, '_blank');
    } else {
        alert('Usuário não logado.');
    }
};

const SoftwareListDrive = () => {
    return (
        <div className="container">
            <div className="header">
                <h1>Softwares</h1>
                <p>Os softwares produzidos pelo GPRHidro são resultado de avançadas pesquisas realizadas ao longo de décadas, sendo advindos de trabalhos de conclusão de curso, dissertações de mestrado, teses de doutorado e artigos científicos. Os softwares estão disponíveis para download no Google Drive.</p>
            </div>
            <div className="grid">
                {softwareList.map((software) => (
                    <div key={software.id} className="software-item">
                        <img src={software.imageUrl} alt={software.name} />
                        <button onClick={() => handleDownload(software.driveUrl, software.name, software.temLicenca)}>Download</button>
                    </div>
                ))}
            </div>
        </div>
    );
};

export default SoftwareListDrive;
