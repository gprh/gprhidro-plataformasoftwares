import React, { useState, useEffect } from 'react';
import { getFirestore, collection, query, where, getDocs } from "firebase/firestore";
import './UserDownloadsList.css';

const UserDownloadsList = () => {
  const [userDownloads, setUserDownloads] = useState([]);

  useEffect(() => {
    const fetchUserDownloads = async () => {
      const firestore = getFirestore();

      // Primeiro, pegamos todos os usuários
      const usersRef = collection(firestore, "users");
      const usersSnapshot = await getDocs(usersRef);
      const users = usersSnapshot.docs.map(doc => ({ id: doc.id, ...doc.data() }));

      // Agora, para cada usuário, buscamos os downloads correspondentes
      const downloadsRef = collection(firestore, "downloads");
      const downloadsSnapshot = await getDocs(downloadsRef);

      // Criamos um mapa onde cada chave é o userId e o valor é uma lista dos softwares baixados
      const downloadsByUser = {};
      downloadsSnapshot.forEach(downloadDoc => {
        const downloadData = downloadDoc.data();
        if (!downloadsByUser[downloadData.userId]) {
          downloadsByUser[downloadData.userId] = [];
        }
        downloadsByUser[downloadData.userId].push(downloadData.softwareName);
      });

      // Combinamos os usuários com seus downloads
      const userDownloadList = users.map(user => ({
        ...user,
        downloads: downloadsByUser[user.id] || []
      }));

      setUserDownloads(userDownloadList);
    };

    fetchUserDownloads();
  }, []);

  return (
    <div className="user-downloads-list">
      <h1>Usuários e Downloads</h1>
      <p>Total de Usuários: {userDownloads.length}</p> {/* Exibe o total de usuários */}
      <table>
        <thead>
          <tr>
            <th>Nome</th>
            <th>Email</th>
            <th>Downloads</th>
          </tr>
        </thead>
        <tbody>
          {userDownloads.map(user => (
            <tr key={user.id}>
              <td>{user.nome}</td>
              <td>{user.email}</td>
              <td>{user.downloads.join(', ')}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
};

export default UserDownloadsList;
