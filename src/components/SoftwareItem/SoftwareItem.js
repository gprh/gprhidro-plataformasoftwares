import React from "react";
import "./SoftwareItem.css";

const SoftwareItem = ({ software }) => {
  return (
    <div className="softwareItem">
      <div className="softwareItem-info">
        <span className="softwareItem-name">{software.softwareName}</span>
        <span className="softwareItem-date">
          Último download:{" "}
          {new Date(software.timestamp.seconds * 1000).toLocaleDateString()}
        </span>
      </div>

      <div className="softwareItem-license">
        {software.temLicenca ? (
          <span className="license-required">🔑 Este software requer uma licença</span>
        ) : (
          <span className="no-license-needed">✅ Este software não requer licença</span>
        )}
      </div>
    </div>
  );
};

export default SoftwareItem;
