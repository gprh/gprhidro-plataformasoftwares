import React, { useState, useEffect } from 'react';
import { getFirestore, collection, getDocs } from "firebase/firestore";
import './UserList.css';

const UserList = () => {
  const [users, setUsers] = useState([]);

  useEffect(() => {
    const fetchUserProfiles = async () => {
      const firestore = getFirestore();
      const userProfilesRef = collection(firestore, "users");
      const querySnapshot = await getDocs(userProfilesRef);
      const userList = querySnapshot.docs.map(doc => ({ id: doc.id, ...doc.data() }));
      setUsers(userList);
    };

    fetchUserProfiles();
  }, []);

  return (
    <div className="user-list">
      <h1>Lista de Usuários</h1>
      <p>Total de Usuários: {users.length}</p> {/* Exibe o total de usuários */}
      <table>
        <thead>
          <tr>
            <th>Nome</th>
            <th>Email</th>
            <th>Titulação Máxima</th>
            <th>Universidade</th>
            <th>Empresa</th>
            <th>Telefone</th>
          </tr>
        </thead>
        <tbody>
          {users.map(user => (
            <tr key={user.id}>
              <td>{user.nome}</td>
              <td>{user.email}</td>
              <td>{user.titulacaoMaxima}</td>
              <td>{user.universidade}</td>
              <td>{user.empresa}</td>
              <td>{user.telefone}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
};

export default UserList;
