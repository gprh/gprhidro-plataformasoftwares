import React, { useState, useEffect } from "react";
import "./LicenseManager.css";
import { getValidationDate } from "../../api/userProfileApi"; // Importa a função do Firebase

const LicenseManager = () => {
  const [userPassword, setUserPassword] = useState("");
  const [licenseKey, setLicenseKey] = useState("");
  const [validDate, setValidDate] = useState("Carregando..."); // Estado para armazenar a data do Firebase

  const SHIFT = 3; // Mesmo deslocamento do Java

  // Buscar a data de validade no Firestore ao carregar o componente
  useEffect(() => {
    const fetchValidDate = async () => {
      const dateFromFirebase = await getValidationDate();
      if (dateFromFirebase) {
        setValidDate(dateFromFirebase);
      } else {
        setValidDate("Erro ao carregar data");
      }
    };

    fetchValidDate();
  }, []);

  // Criptografar a data usando SHIFT (igual ao Java)
  const criptografarData = (data) => {
    return data
      .split("")
      .map((c) => String.fromCharCode(c.charCodeAt(0) + SHIFT))
      .join("");
  };

  // Função principal para gerar a ContraSenha (igual ao Java)
  const generateLicenseKey = (userPassword, dataValidade) => {
    const dataCriptografada = criptografarData(dataValidade);
    console.log("dataCript:"+dataCriptografada);
    return criptografarData(`${userPassword}-${dataCriptografada}`);
  };

  const handleGenerateClick = () => {
    if (!userPassword.trim()) {
      alert("Digite o ID único da máquina para continuar.");
      return;
    }

    if (validDate === "Erro ao carregar data" || validDate === "Carregando...") {
      alert("Erro ao obter a data de validade.");
      return;
    }

    // Usa a data buscada no Firebase para gerar a licença
    const license = generateLicenseKey(userPassword, validDate);
    setLicenseKey(license);
  };

  return (
    <div className="licenseManager">
      <h2>Gerador de Licença</h2>
      <p><strong>Validade:</strong> {validDate}</p>
      <input
        type="text"
        placeholder="Digite o ID Único"
        value={userPassword}
        onChange={(e) => setUserPassword(e.target.value)}
        className="license-input"
      />
      <button className="confirm-btn" onClick={handleGenerateClick}>
        Gerar Licença
      </button>
      {licenseKey && (
        <input
          type="text"
          value={licenseKey}
          readOnly
          className="license-result"
          onClick={(e) => e.target.select()} // Facilita a cópia
        />
      )}
    </div>
  );
};

export default LicenseManager;
